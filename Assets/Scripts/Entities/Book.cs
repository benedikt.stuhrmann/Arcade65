﻿using UnityEngine;

/**
 * Repräsentiert ein Zauberbuch bzw. eine Schriftrolle (unterscheidung lediglich in Magiewert und Grafik) mit einem bestimmten Magiewert.
 * Wird ein Buch erstellt, muss über SetMagicType der Magietyp für das Buch festgelegt werden. 
 * Das Buch hat eine X% Chance, dass das Buch totzdem den andren Magietyp hat, als gewüscht.
 */
public class Book : MonoBehaviour {

    // Der Magie Typ des Buches
    private Enums.MagicType magicType;
    // Magiewert, welchen das Buch einem Zauberer gibt
    private int wisdom;
    // Der allgemeine Typ des Buches
    private Enums.BookType bookType;
    [SerializeField]
    private float mutationChance = 0.15f;
    [SerializeField]
    private float scrollChance = 0.10f;
    [SerializeField]
    private int[] scrollWisdomRange = { 16, 30 };
    [SerializeField]
    private int[] bookWisdomRange = { 5, 15 };
    private int spawnIndex;
    private Mage reader;

    // Variablen, um bewegen zu ermøglichen
    private bool move;
    private bool moveEnd;
    private Vector3 target;
    private GameObject[] waypoints;
    private float bezierTime = 0;
    private Vector3 startPoint;
    [SerializeField]
    private float speed = 1.0f;

    public void Initiate() {
        this.SetBookType(Enums.BookType.Book, false);
    }

    public void SetMagicType(Enums.MagicType _magicType, bool overwrite) {
        if (!overwrite) {
            // Prozentuale Chance, dass das Buch den anderen Magietyp hat
            if (Random.value <= this.mutationChance) {
                if (_magicType == Enums.MagicType.Dark) {
                    _magicType = Enums.MagicType.Light;
                } else {
                    _magicType = Enums.MagicType.Dark;
                }
            }
        }
        this.magicType = _magicType;

        // Einfärbung basierend auf Magietyp
        Color color;
        if (this.magicType == Enums.MagicType.Dark) {
            color = Color.red;
        } else {
            color = Color.green;
        }

        Renderer renderer = GetComponent<Renderer>();
        renderer.material.SetColor("_Color", color);
    } // end SetMagicType

    public void SetBookType(Enums.BookType _bookType, bool overwrite) {
        System.Random rnd = Enums.GlobalObjects.randomInstance;
        if (!overwrite) {
            if (Random.value <= this.scrollChance) {
                this.wisdom = rnd.Next(this.scrollWisdomRange[0], this.scrollWisdomRange[1] + 1);
                this.bookType = Enums.BookType.Scroll;
            } else {
                this.wisdom = rnd.Next(this.bookWisdomRange[0], this.bookWisdomRange[1] + 1);
                this.bookType = Enums.BookType.Book;
            }
        } else {
            this.bookType = _bookType;
        }
    }

    public Enums.MagicType GetMagicType() {
        return this.magicType;
    }

    public void SetWisdom(int newWisdom) {
        this.wisdom = newWisdom;
    }

    public int GetWisdom(){
        return this.wisdom;
    }

    public Enums.BookType GetBookType() {
        return this.bookType;
    }

    public void SetSpawnIndex(int spawnIndex) {
        this.spawnIndex = spawnIndex;
    } // end SetSpawnIndex

    public int GetSpawnIndex() {
        return this.spawnIndex;
    } // end GetSpawnIndex

    public override string ToString() {
        return "Book of type " + this.bookType + " has " + this.wisdom + " wisdom";
    }

    public void StartMoving(Vector3 target, string magicType) {
        this.waypoints = GameObject.FindGameObjectsWithTag("BookWaypoint" + magicType);
        this.startPoint = this.transform.position;
        this.move = true;
        this.target = target;
    }

    private void Update() { 
        // Moves the book from the shelf close to the target position
        if(this.move) {
            float step = 3 * Time.deltaTime;
            // Rotates the book with a constant speed. Must be higher when working with higher speeds.
            if (this.transform.rotation.x < 0.5f) {
                this.transform.Rotate(Vector3.right * step * (35 * (1/this.speed)));
            }

            this.bezierTime = this.bezierTime + Time.deltaTime / (1/this.speed);

            float curveX = (((1 - this.bezierTime) * (1 - this.bezierTime)) * this.startPoint.x) + (2 * this.bezierTime * (1 - this.bezierTime) * waypoints[0].transform.position.x) + ((this.bezierTime * this.bezierTime) * target.x);
            float curveY = (((1 - this.bezierTime) * (1 - this.bezierTime)) * this.startPoint.y) + (2 * this.bezierTime * (1 - this.bezierTime) * waypoints[0].transform.position.y) + ((this.bezierTime * this.bezierTime) * target.y);
            float curveZ = (((1 - this.bezierTime) * (1 - this.bezierTime)) * this.startPoint.z) + (2 * this.bezierTime * (1 - this.bezierTime) * waypoints[0].transform.position.z) + ((this.bezierTime * this.bezierTime) * target.z);
            transform.position = new Vector3(curveX, curveY, curveZ);

            // If the book almost reached its target, move it slowly straight down (see if for this.moveEnd)
            if(Vector3.Distance(target, this.transform.position) < 0.6f) {
                this.move = false;
                this.moveEnd = true;
            }
        }

        // Moves the book straight down for the last few bits
        if(this.moveEnd) {
            float step = 5 * Time.deltaTime;
            this.transform.position = Vector3.MoveTowards(this.transform.position, target, step);
            if(this.transform.position == target) {
                this.moveEnd = false;
            }
        }
    }

    public Mage GetReader() {
        return this.reader;
    }

    public void SetReader(Mage newReader) {
        this.reader = newReader;
    }
} // end Book
