﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class Aura : MonoBehaviour {

    [SerializeField]
    private Color mainColorLightMagic = new Color(0.2f, 1f, 0f, 1f);
    [SerializeField]
    private Color mainColorDarkMagic = new Color(0.8588235f, 0f, 0.8431373f, 1f);
    private Color mainColorActive;

    private MagicType activeMagicType;
    [SerializeField]
    private int maxWisdom = 100;
    // Wert zwischen 0 und 1, welcher prozentualen Magiewert wiedergibt (0 = min. Magiewert, 1 = max. Magiewert)
    [SerializeField]
    private float wisdom = 0f;

    // Einzelne Partikelsysteme des Aura Effekts
    private List<ParticleSystem> effects;

    // TODO: Näcgsteb beiden Variabln beim Einbauen entfernen, da nur zum Testen
    public bool fakeWisdomChange = false;
    public int fakedWisdom = 0;

    void Awake() {
        // Einzelne Partikelsysteme abspeichern, für späteren Zugriff
        foreach (Transform child in this.transform.GetComponentsInChildren<Transform>()) {
            if (child.CompareTag("Aura")) {
                this.effects = new List<ParticleSystem>(child.GetComponentsInChildren<ParticleSystem>());
                this.effects.RemoveAt(0);
                break;
            }
        }
    } // end Awake

    // TODO: Beim Einbauen entfernen, da nur zum Testen
    private void Start() {
        SetWisdom(MagicType.Light, fakedWisdom);
        if (this.fakeWisdomChange) {
            StartCoroutine("ChangeWisdom");
        }
    }

    // TODO: Beim Einbauen entfernen, da nur zum Testen
    private IEnumerator ChangeWisdom() {
        int seconds = 10;
        yield return new WaitForSeconds(seconds);
        int newAmount = Mathf.RoundToInt(this.wisdom * 100f) + 20;
        SetWisdom(MagicType.Light, newAmount);
        if (this.wisdom != 1) StartCoroutine("ChangeWisdom");
    }

    /// <summary>
    /// Setzt den Wert des angegebenen MagieTypen auf den angegebenen Wert und passt dementsprechend den Aura Effekt an.
    /// </summary>
    /// <param name="type"> Der Magietyp, für welchen der Wert gesetzt werden soll</param>
    /// <param name="amount"> Der Wert, welcher gesetzt werden soll</param>
    public void SetWisdom(MagicType type, int amount) {
        this.activeMagicType = type;
        amount = Mathf.Clamp(amount, 0, this.maxWisdom);

        switch (this.activeMagicType) {
            case MagicType.Light :
                this.mainColorActive = this.mainColorLightMagic;
                break;
            case MagicType.Dark :
                this.mainColorActive = this.mainColorDarkMagic;
                break;
            default:
                this.wisdom = 0f;
                this.ToggleAuraEffect(false);
                return;
        }

        if (this.wisdom == 0 && amount > 0) {
            // Effekt war aus => einschalten
            this.ToggleAuraEffect(true);
        }

        // Magiewert anpassen
        float amountPercent = 100 / this.maxWisdom * amount;
        this.wisdom = Mathf.Clamp01(amountPercent/100);

        // Wenn beide Magiewerte 0 sind, MagicType auf None setzen => deaktiviert Aura Effekt komplett
        if (this.wisdom == 0f) {
            this.activeMagicType = MagicType.None;
        }

        this.UpdateAuraEffect();
    } // end SetWisdom

    /// <summary>
    /// Aktiviert/Deaktiviert den Aura Effekt.
    /// </summary>
    /// <param name="enabled"> true = Aura Effekt an, false = Aura Effekt aus</param>
    private void ToggleAuraEffect(bool enabled) {
        // TODO: NullReferenceException: Object reference not set to an instance of an object
        foreach (ParticleSystem effect in this.effects) {
            ParticleSystem.EmissionModule emission = effect.emission;
            emission.enabled = enabled;
        }
    } // end ToggleAuraEffect

    /// <summary>
    /// Passt den Aura Effekt, entsprechend des Magiewertes, des aktuellen Magietyps an.
    /// </summary>
    private void UpdateAuraEffect() {
        if (this.activeMagicType == MagicType.None) {
            // Mage ist neutral => Aura Effekt deaktivieren
            this.ToggleAuraEffect(false);
            return;
        }

        foreach (ParticleSystem effect in this.effects) {
            // Einzelne Effekte anpassen
            switch (effect.name) {
                case "Circle":
                    this.UpdateCircleEffect(effect);
                    break;
                case "Particles":
                    this.UpdateParticlesEffect(effect);
                    break;
                case "Electric Beam":
                    this.UpdateElectricBeamEffect(effect);
                    break;
                case "Smoke":
                    this.UpdateSmokeEffect(effect);
                    break;
                case "Update":
                    this.VisualizeWisdomLevelChanged(effect);
                    break;
            }
        }
    } // end UpdateAuraEffect

    private void UpdateCircleEffect(ParticleSystem effect) {
        ParticleSystem.MainModule main = effect.main;

        if (this.Between(this.wisdom, 0f, 0.2f)) {
            // Stage 1
            this.mainColorActive.a = 0.2f;
            
        } else if (this.Between(this.wisdom, 0.2f, 0.4f)) {
            // Stage 2
            this.mainColorActive.a = 0.4f;

        } else if (this.Between(this.wisdom, 0.4f, 0.6f)) {
            // Stage 3
            this.mainColorActive.a = 0.75f;

        } else if (this.Between(this.wisdom, 0.6f, 0.8f)) {
            // Stage 4
            this.mainColorActive.a = 1f;

        } else if (this.Between(this.wisdom, 0.8f, 1f)) {
            // Stage 5
            this.mainColorActive.a = 1f;

        }

        main.startColor = this.mainColorActive;
    } // end UpdateCircleEffect

    private void UpdateParticlesEffect(ParticleSystem effect) {
        ParticleSystem.MainModule main = effect.main;
        ParticleSystem.EmissionModule emission = effect.emission;

        if (this.Between(this.wisdom, 0f, 0.2f)) {
            // Stage 1
            this.mainColorActive.a = 0.5f;
            main.startLifetime = new ParticleSystem.MinMaxCurve(0.2f, 0.3f);
            emission.rateOverTime = 150;

        } else if (this.Between(this.wisdom, 0.2f, 0.4f)) {
            // Stage 2
            this.mainColorActive.a = 0.75f;
            main.startLifetime = new ParticleSystem.MinMaxCurve(0.3f, 0.5f);
            main.startSize = new ParticleSystem.MinMaxCurve(0.1f, 0.4f);

        } else if (this.Between(this.wisdom, 0.4f, 0.6f)) {
            // Stage 3
            this.mainColorActive.a = 0.75f;
            main.startLifetime = new ParticleSystem.MinMaxCurve(0.3f, 0.55f);
            main.startSize = new ParticleSystem.MinMaxCurve(0.1f, 0.4f);

        } else if (this.Between(this.wisdom, 0.6f, 0.8f)) {
            // Stage 4
            this.mainColorActive.a = 0.6f;
            main.startLifetime = new ParticleSystem.MinMaxCurve(0.4f, 0.6f);
            main.startSize = new ParticleSystem.MinMaxCurve(0.2f, 0.4f);
            emission.rateOverTime = 150;

        } else if (this.Between(this.wisdom, 0.8f, 1f)) {
            // Stage 5
            this.mainColorActive.a = 1f;
            main.startLifetime = new ParticleSystem.MinMaxCurve(0.5f, 0.7f);
            main.startSize = new ParticleSystem.MinMaxCurve(0.1f, 0.5f);
            emission.rateOverTime = 200;
        }

        main.startColor = new ParticleSystem.MinMaxGradient(this.Darken(this.mainColorActive, 0.4f), this.mainColorActive);
    } // end UpdateParticlesEffect

    private void UpdateElectricBeamEffect(ParticleSystem effect) {
        ParticleSystem.MainModule main = effect.main;

        if (this.Between(this.wisdom, 0f, 0.2f)) {
            // Stage 1
            main.startLifetime = 50f;
            main.startSize = new ParticleSystem.MinMaxCurve(1f, 1.2f);

        } else if (this.Between(this.wisdom, 0.2f, 0.4f)) {
            // Stage 2
            main.startLifetime = 2f;
            main.startSize = new ParticleSystem.MinMaxCurve(2f, 2.5f);

        } else if (this.Between(this.wisdom, 0.4f, 0.6f)) {
            // Stage 3
            main.startLifetime = 2f;
            main.startSize = new ParticleSystem.MinMaxCurve(2f, 2.5f);

        } else if (this.Between(this.wisdom, 0.6f, 0.8f)) {
            // Stage 4
            main.startLifetime = 2f;
            main.startSize = new ParticleSystem.MinMaxCurve(2f, 2.5f);

        } else if (this.Between(this.wisdom, 0.8f, 1f)) {
            // Stage 5
            main.startLifetime = 2f;
            main.startSize = new ParticleSystem.MinMaxCurve(2f, 2.5f);

        }

        main.startColor = this.mainColorActive;
    } // end UpdateElectricBeamEffect

    private void UpdateSmokeEffect(ParticleSystem effect) {
        ParticleSystem.MainModule main = effect.main;

        if (this.Between(this.wisdom, 0f, 0.2f)) {
            // Stage 1
            main.startLifetime = new ParticleSystem.MinMaxCurve(0.3f, 0.5f);
            main.startSpeed = new ParticleSystem.MinMaxCurve(0.005f, 0.2f);
            this.mainColorActive.a = 0.2f;

        } else if (this.Between(this.wisdom, 0.2f, 0.4f)) {
            // Stage 2
            main.startLifetime = new ParticleSystem.MinMaxCurve(1f, 1f);
            main.startSpeed = new ParticleSystem.MinMaxCurve(0.005f, 0.5f);
            this.mainColorActive.a = 0.15f;

        } else if (this.Between(this.wisdom, 0.4f, 0.6f)) {
            // Stage 3
            main.startLifetime = new ParticleSystem.MinMaxCurve(1f, 1f);
            main.startSpeed = new ParticleSystem.MinMaxCurve(0.005f, 0.5f);
            this.mainColorActive.a = 0.15f;

        } else if (this.Between(this.wisdom, 0.6f, 0.8f)) {
            // Stage 4
            main.startLifetime = new ParticleSystem.MinMaxCurve(1f, 1f);
            main.startSpeed = new ParticleSystem.MinMaxCurve(0.005f, 0.5f);
            this.mainColorActive.a = 0.15f;

        } else if (this.Between(this.wisdom, 0.8f, 1f)) {
            // Stage 5
            main.startLifetime = new ParticleSystem.MinMaxCurve(1f, 1f);
            main.startSpeed = new ParticleSystem.MinMaxCurve(0.005f, 0.5f);
            this.mainColorActive.a = 0.2f;
        }

        main.startColor = new ParticleSystem.MinMaxGradient(this.Darken(this.mainColorActive, 0.4f), this.mainColorActive);
    } // end UpdateSmokeEffect


    private void VisualizeWisdomLevelChanged(ParticleSystem effect) {
        ParticleSystem.MainModule main = effect.main;
        this.mainColorActive.a = 0.7f;
        main.startColor = this.mainColorActive;

        effect.Emit(1);
    } // end VisualizeWisdomLevelChanged

    /// <summary>
    /// Gibt zurück, ob der angegebene Wert zwischen der angegebenen unteren (nicht mit eingeschlossen) und oberen Grenze (eingeschlossen) liegt.
    /// </summary>
    /// <param name="value"> Der zu überprüfende Wert</param>
    /// <param name="lowerBound"> Untere Grenze (ausgeschlossen)</param>
    /// <param name="upperBound"> Obere Grenze (eingeschlossen)</param>
    /// <returns>Wert liegt zwischen unterer und oberer Grenze</returns>
    private bool Between(float value, float lowerBound, float upperBound) {
        return value > lowerBound && value <= upperBound;
    } // end Between

    /// <summary>
    /// Verdunkelt die Farbe um den angegebenen Prozentwert
    /// </summary>
    /// <param name="color">Ausgangfarbe, die verdunkelt werden soll</param>
    /// <param name="percent">Prozentangabe, um wieviel Prozent die Ausgangsfarbe verdunkelt werden soll</param>
    /// <returns>Verdunkelte Farbe</returns>
    private Color Darken(Color color, float percent) {
        return new Color(color.r * (1 - percent), color.g * (1 - percent), color.b * (1 - percent), color.a);
    } // end Darken

} // end Aura
