﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breathe : MonoBehaviour {

    [SerializeField]
    private Vector3 increaseBy = new Vector3(0.2f, 0f, 0.3f);
    [SerializeField]
    private float speed = 0.01f;

    private Vector3 originalSize;
    private Vector3 maxScale;
    private bool growing = true;
    private float progress;

    void Start(){
        this.originalSize = this.transform.localScale;
        this.maxScale = this.originalSize + this.increaseBy;
    } // end Start

    void Update() {
        this.progress = Mathf.Clamp01(this.progress + this.speed);

        if (growing) {
            // größer werden
            this.transform.localScale = Vector3.Lerp(this.originalSize, this.maxScale, progress);
        } else {
            // kleiner werden
            this.transform.localScale = Vector3.Lerp(this.maxScale, this.originalSize, progress);
        }
        if (this.progress == 1) {
            this.growing = !this.growing;
            this.progress = 0;
        }

    } // end Update

} // end Breathe
