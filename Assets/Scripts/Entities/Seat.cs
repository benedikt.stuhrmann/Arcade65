﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class Seat : MonoBehaviour, ITouchable {
    
    private MagicSchool magicSchool;
    [SerializeField]
    private int freeUpSeatIndex = 0;

    void Start() {
        this.magicSchool = GetComponentInParent<MagicSchool>();
    } // end Start

    void Update() {

    } // end Update()

    public TouchActionHandler touchActionHandler { get; set; }

    public void isTouched(TouchInteraction interaction) {
        // nothing
    } // end isTouched

    public void wasTouched(TouchInteraction interaction) {
        switch (interaction.touchType) {
            case TouchType.SwipeEast :
                if (this.magicSchool.GetMagicType() == MagicType.Light) {
                    this.magicSchool.KickOut(this.freeUpSeatIndex);
                }
                break;
            case TouchType.SwipeWest:
                if (this.magicSchool.GetMagicType() == MagicType.Dark) {
                    this.magicSchool.KickOut(this.freeUpSeatIndex);
                }
                break;
        }
    } // end wasTouched

} // end Seat
