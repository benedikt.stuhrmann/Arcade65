﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookTouchBehavior : MonoBehaviour,ITouchable {
    public TouchActionHandler touchActionHandler
    {
        get;
        set;
    }

    private bool isDragged = false;
    private Vector3 startPosition;
    public MagicSchool relatedSchool;
    private bool homing = false;
    private GameObject relatedBook;
    private Vector3 relatedBookSpawnPos;
    [SerializeField]
    private float homingSpeed = 10f;
    private GameObject fireplace;

    // Use this for initialization
    void Start() {
        touchActionHandler = new TouchActionHandler();
        startPosition = this.transform.position;
        // Fireplace setzen
        foreach (Transform child in this.relatedSchool.GetComponentsInChildren<Transform>()) {
            if (child.name == "Fireplace") {
                this.fireplace = child.gameObject;
                break;
            }
        }
    }

    // Update is called once per frame
    void Update() {
        if (this.homing) {
            // Buch an seinen Spawnpunkt zurückfliegen lassen
            float step = Time.deltaTime * this.homingSpeed;
            this.relatedBook.transform.position = Vector3.MoveTowards(this.relatedBook.transform.position, this.relatedBookSpawnPos, step);

            if (this.relatedBook.transform.position == this.relatedBookSpawnPos) {
                // Ziel erreicht
                this.homing = false;
                this.transform.position = this.startPosition; // Touch Plane zurücksetzen
            }
        }
    }

    public void isTouched(TouchInteraction interaction)
    {
        this.relatedBook = this.GetRelatedBook();

        if (!isDragged)
        {
            interaction.inUseBy = this.gameObject;
            goHigher();
        }

        isDragged = true;
        touchActionHandler.dragObject(this.gameObject, interaction);
        touchActionHandler.dragObject(this.relatedBook, interaction);
    }

    public void wasTouched(TouchInteraction interaction) {
        this.isDragged = false;
        // avoid that interaction is used again 
        interaction.inUseBy = null;
        this.goLower();

        if (this.BookHitsFireplace()) {
            // Buch zerstören
            this.relatedSchool.DestroyBook(this.relatedBook);
            this.transform.position = this.startPosition;
            return;
        }

        foreach (Mage mage in this.relatedSchool.getInsideMages()) {
            if (mage != null && this.BookHitsMage(mage)) {
                mage.SendMessage("TakeBook", this.relatedBook.GetComponent<Book>());

                // Buch zum jeweiligem Punkt bewegen
                this.relatedBook.GetComponent<Book>().SetReader(mage);
                this.relatedSchool.MoveBookToReadPoint(this.relatedBook);
                this.transform.position = this.startPosition;

                return;
            }
        }

        // Buch hat keinen Zauberer getroffen
        // deswegen zurück an seinen Platz auf der Theke fliegen lassen
        this.relatedBookSpawnPos = (Vector3) this.relatedSchool.GetBookSpawnPosRot(this.relatedBook.GetComponent<Book>().GetSpawnIndex())[0];
        this.homing = true;
    }

    private bool BookHitsMage(Mage mage) {
        Collider mageCollider = mage.gameObject.GetComponent<Collider>();
        Collider bookCollider = this.transform.parent.GetComponentInChildren<Book>().GetComponent<Collider>();
        return mageCollider.bounds.Intersects(bookCollider.bounds);
    }

    private bool BookHitsFireplace() {
        Collider fireplaceCollider = this.fireplace.GetComponent<Collider>();
        Collider bookCollider = this.transform.parent.GetComponentInChildren<Book>().GetComponent<Collider>();
        return fireplaceCollider.bounds.Intersects(bookCollider.bounds);
    }

    private void goHigher()
    {
        transform.position += new Vector3(0, 3, 0);
        this.relatedBook.transform.position += new Vector3(0, 3, 0);
    }

    private void goLower()
    {
        transform.position += new Vector3(0, -1f, 0);
        this.relatedBook.transform.position += new Vector3(0, -1f, 0);
    }

    private GameObject GetRelatedBook() {
        return this.transform.parent.GetComponentInChildren<Book>().gameObject;
    }

}
