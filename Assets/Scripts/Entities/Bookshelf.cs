﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class Bookshelf : MonoBehaviour {

    private float capacity;
    private ShelfLevel[] levels;
    private float fillStatus;
    [SerializeField]
    private GameObject bookPrefab;
    private MagicSchool magicSchool;

    public void Initiate() {
        this.magicSchool = this.transform.parent.gameObject.GetComponentInChildren<MagicSchool>();
        this.FillLevels();
    }

    private void FillLevels() {
        Transform[] children = this.transform.GetComponentsInChildren<Transform>();
        List<Transform> levelGOs = new List<Transform>();
        foreach (Transform child in children) {
            if (child.tag == "ShelfLevel") {
                levelGOs.Add(child);
            }
        }
        this.levels = new ShelfLevel[levelGOs.Count];
        int counter = 0;
        foreach (Transform level in levelGOs) {
            this.levels[counter] = new ShelfLevel(level.transform.position, level.transform.localScale.x, this);
            this.levels[counter].SetMagicType(this.magicSchool.GetMagicType());
            this.levels[counter].SetBookPrefab(this.bookPrefab);
            this.levels[counter].Initiate();
            this.capacity += this.levels[counter].GetCapacity();
            this.fillStatus += this.levels[counter].GetFillStatus();
            counter++;
        }
    }

    public GameObject TakeBook() {
        foreach(ShelfLevel level in this.levels) {
            if(level.GetFillStatus() > 0) {
                GameObject takenBook = level.TakeBook();
                this.fillStatus--;
                return takenBook;
            }
        }
        return null;
    }

    /**
     * Klasse repräsentiert ein Stockwerk des Bücherregals. Aufgrund der Daten dieses Stockwerks werden die Positionen der Bücher errechnet und dann mit Büchern befüllt.
     * Jedes Stockwerk verwaltet dann seinen eigenen Status und seine eigenen Bücher. 
     * */
    public class ShelfLevel {
        private float capacity;
        private float fillStatus;
        private Vector3 position;
        private float width;
        private OrderedDictionary booksInLevel;
        private GameObject bookPrefab;
        private Bookshelf parent;
        private Enums.MagicType magicType;

        public ShelfLevel(Vector3 _position, float _width, Bookshelf _parent) {
            this.position = _position;
            this.width = _width;
            this.parent = _parent;
        }

        public void Initiate() {
            this.booksInLevel = new OrderedDictionary();
            float scaling = 0.5f;
            Vector3 bookPrefabScale = this.bookPrefab.transform.localScale;
            if(bookPrefabScale != null) {
                this.capacity = System.Convert.ToInt32(System.Math.Floor(this.width / (bookPrefabScale.x / 2)));
            }
            for(int i = 0; i < this.capacity; i++) {
                float x = (this.position.x - (this.width / (2 / scaling))) + (bookPrefabScale.x / (4 / scaling)) + i * (bookPrefabScale.x / (2 / scaling)); // Äußerst linke Seite des Regals + 1/8 der dicke des Buchs (damit Buch nicht in der Wand steckt) + die Position des zu spawnenden Buchs
                float y = this.position.y + (bookPrefabScale.y / (2 / scaling)) + (0.1f * Mathf.Pow(scaling, 2)); // Die Position des Levels + (die halbe Höhe des Buches + die halbe Dicke des Levels)
                float z = this.position.z + 0.2f; // Die Position des Levels - einer minimalen Verschiebung für bessere Sichtbarkeit
                if (parent.magicSchool.GetMagicType() == Enums.MagicType.Light) {
                    z = this.position.z - 0.2f;
                }
                Vector3 bookPosition = new Vector3(x, y, z);

                // Neues Buch an der errechneten Position spawnen und zum Dictionary hinzufügen
                GameObject newBook = Instantiate(this.bookPrefab, bookPosition, Quaternion.identity, this.parent.transform);
                newBook.GetComponent<Book>().Initiate();
                newBook.GetComponent<Book>().SetMagicType(this.magicType, false);
                newBook.transform.Rotate(Vector3.up * 90);
                this.booksInLevel.Add(bookPosition, newBook);
                this.fillStatus++;
            }
        }

        public GameObject TakeBook() {
            foreach(DictionaryEntry kvp in this.booksInLevel) {
                Vector3 bookPos = (Vector3)kvp.Key;
                GameObject book = (GameObject)kvp.Value;
                if(book != null) {
                    this.fillStatus--;
                    GameObject takenBook = book;
                    this.booksInLevel[bookPos] = null;
                    Destroy(book);
                    return takenBook;      
                }
            }
            return null;
        }

        public void SetBookPrefab(GameObject bookPrefab) {
            this.bookPrefab = bookPrefab;
        }

        public void SetMagicType(Enums.MagicType newMagicType) {
            this.magicType = newMagicType;
        }

        public float GetCapacity() {
            return this.capacity;
        }

        public float GetFillStatus() {
            return this.fillStatus;
        }

        public override string ToString() {
            return "Level mit Kapazität " + this.capacity + " ist gefüllt mit " + this.fillStatus + " Büchern und hat Magie " + this.magicType;
        }
    }
}