﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class Mage : MonoBehaviour {

	private MagicSchool darkSchool;
	private MagicSchool lightSchool;
	private MagicSchool choosenSchool;
	private MagicType magicType = MagicType.None;
	private MageStatus mageStatus;
    [SerializeField]
    private int maxWisdom = 100;
    // Wisdom Wert kann zwischen -maxWisdom und maxWisdom liegen
    // negativer Wert = dunkle Magie, 0 = neutral, positiver Wert = helle Magie
    [SerializeField]
	private int wisdom = 0;

	private float boredomTimer = 10f;
	private float readTimer = 10f;
	private float spellTimer = 5f;
    private int seatIndex;
	private bool timerStopped = true;
    [SerializeField]
    private float timer;
    private Aura aura;
    private Book book = null;
    private ThoughtBubbles bubbles;

    [SerializeField]
    private WorldManager worldManager; 

	// Use this for initialization
	void Start() {
        this.aura = this.GetComponent<Aura>();
        this.bubbles = this.GetComponent<ThoughtBubbles>();

        darkSchool = GameObject.FindGameObjectWithTag("Magic School Dark").GetComponent<MagicSchool>();
		lightSchool = GameObject.FindGameObjectWithTag("Magic School Light").GetComponent<MagicSchool>();
        this.worldManager = this.worldManager ?? GameObject.FindObjectOfType<WorldManager>();

        // TODO: Nicht random entscheiden, sondern von Welt bekommen, wo frei ist
        this.choosenSchool = this.worldManager.ChooseSchool();

        StartBoredomTimer();
        this.choosenSchool.CanIEnter(this);
    }

	// Update is called once per frame
	void Update() {

		if (!timerStopped) {
			UpdateTimer();
		}

		if (IsOverload()) {
			Overload();
		}
	}

	public void GoTo(Vector3 pos) {
        this.StopTimer();

        this.transform.position = pos;
        if (pos.x > 0) {
            this.transform.Rotate(new Vector3(0, 90, 0));
        } else {
            this.transform.Rotate(new Vector3(0, -90, 0));
        }

        this.bubbles.Show(this, ThoughtBubbleType.WantBook, Random.Range(3000, 5000));
        this.StartBoredomTimer();
    }

	public void Leave() {
        if (this.magicType == this.choosenSchool.GetMagicType()) {
            StopTimer();
            this.choosenSchool.FreeUpSeat(this);
        } else {
            Debug.Log("Can't leave because of wrong MagicType");
        }
    }

    /// <summary>
    /// Gibt ABSOLUTEN Wisdomwert des aktuellen Magietyps zurück
    /// </summary>
    /// <returns>Absoluter Wisdomwert</returns>
    private int getCurrentWisdom() {
	    return Mathf.Abs(this.wisdom);
	}

	// TODO Animation fürs hinsetzen triggern
	public void SitDown() {

		StartBoredomTimer();
	}


	public void SwitchSchool() {

		if (choosenSchool == darkSchool) {
			choosenSchool = lightSchool;
		} else {
			choosenSchool = darkSchool;
		}
	}

	public void TakeBook(Book book) {
        this.StopTimer();
        this.book = book;
        this.bubbles.Hide(this);
        this.StartReadTimer();
	}

	private void CastSpell() {
        int damage = getCurrentWisdom();
        if (damage == 0) damage = 5;

        this.choosenSchool.TakeDamage(damage);
        StartSpellTimer();
    }

	public bool IsOverload() {
        return (this.wisdom == this.maxWisdom || this.wisdom == -this.maxWisdom);
	}

	private void Overload() {

		this.choosenSchool.TakeDamage(200);
		Leave();
	}

	private void UpdateTimer() {

		timer -= Time.deltaTime;
		//Debug.Log("My timer is: " + timer);

		if (timer <= 0) {
			StopTimer();
			switch (mageStatus) {
				case MageStatus.Bored:
					CastSpell();
                    // NUR ZUM TESTEN
                    // NUR ZUM TESTEN
                    // Schleife wegen neuem 3D Model
                    foreach (Renderer rend in GetComponentsInChildren<Renderer>()) {
                        rend.material.SetColor("_Color", Color.blue);
                    }
                    break;

				case MageStatus.Reading:
                    // ist fertig mit Lesen des Buchs
                    this.FinishedReading();
					break;

				case MageStatus.Casting:
                    foreach (Renderer rend in GetComponentsInChildren<Renderer>()) {
                        rend.material.SetColor("_Color", Color.blue);
                    }
                    CastSpell();
					break;
			}
		}

	}

	private void StopTimer() {
        foreach (Renderer rend in GetComponentsInChildren<Renderer>()) {
            rend.material.SetColor("_Color", Color.grey);
        }
        timerStopped = true;
		//Debug.Log("Timer stopped!");
	}

	private void StartTimer(float currentTimer) {

		timer = currentTimer;
		timerStopped = false;
		//Debug.Log("Timer started! " + currentTimer);
	}

	// TODO Animation fuer Denkblasen triggern
	public void StartBoredomTimer() {

        float currentTimer = boredomTimer;
		// je größer der Faktor, desto kürzer der Timer
		float factor = getCurrentWisdom() / 20;
		mageStatus = MageStatus.Bored;
		if (factor > 0) {
			currentTimer = boredomTimer / factor ;
		}
		StartTimer(currentTimer);
	}

	// TODO Animation fuer Lesen
	public void StartReadTimer() {
		mageStatus = MageStatus.Reading;
        StartTimer(readTimer);
	}
	
	// TODO Animation fuer Zaubern
	public void StartSpellTimer() {

		mageStatus = MageStatus.Casting;
		StartTimer(spellTimer);
		
	}

    /// <summary>
    /// Aktualisiert den Wisdomwert und den Magietypen des Magiers.
    /// Triggert außerdem ein Update für den Aura Effekt.
    /// </summary>
    private void FinishedReading() {
        if (this.book.GetMagicType() == MagicType.Dark) {
            this.wisdom -= this.book.GetWisdom();
        } else {
            this.wisdom += this.book.GetWisdom();
        }

        if (this.wisdom > 0) {
            this.magicType = MagicType.Light;
        } else if (this.wisdom < 0) {
            this.magicType = MagicType.Dark;
        } else {
            this.magicType = MagicType.None;
        }

        this.aura.SetWisdom(this.magicType, Mathf.Abs(this.wisdom));
        this.choosenSchool.FinishAndDestroyBook(this.seatIndex);
        this.book = null;
        this.bubbles.Show(this, ThoughtBubbleType.WantBook, Random.Range(3000, 5000));
        StartBoredomTimer();
    } // end FinishedReading

    public int GetSeatIndex() {
        return this.seatIndex;
    }

    public void SetSeatIndex(int newIndex) {
        this.seatIndex = newIndex;
    }
}
