﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class ThoughtBubbles : MonoBehaviour {

    [SerializeField]
    private GameObject thoughtBubblePrefabSprite;
    [SerializeField]
    private GameObject thoughtBubblePrefabObject;

    public bool use3Dobject = false;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        
	}

    /**
     * Zeigt eine Denkblase über dem Kopf des angegebenen Magiers für time ms an.
     * Nachdem die Zeit abgelaufen ist, verschwindet die Denkblase wieder.
     * Wenn keine Zeit (oder -1) angegeben wird, existiert die Denkblase unendlich lange, bis sie manuell zerstört wird Hide()
     * oder sie von einer neuen Denkblase üverschrieben wird.
     * Type gibt an, von welchem Typ die Denkblase sein soll (definiert in Enum ThoughtBubbleType)
     */
    public void Show(Mage mage, ThoughtBubbleType type, float time = -1) {
        Vector3 magePos = mage.transform.position;

        // überprüfe ob der Magier bereits eine Denkblase hat
        // wenn ja, lösche diese
        StartCoroutine(this.DestroyBubbles(mage));

        if (!this.use3Dobject) {
            // lade Icon
            Sprite icon = Resources.Load<Sprite>(Path.GRAPHICS.ToString() + type);
            if (icon == null) return;

            // erstelle die neue Denkblase
            Vector3 offset = new Vector3(0.6f, 1.25f, 0f);
            Transform thoughtBubbleContent = Instantiate(this.thoughtBubblePrefabSprite, magePos + offset, Quaternion.Euler(90, 0, 180), mage.transform).transform.GetChild(0);
            thoughtBubbleContent.GetComponent<SpriteRenderer>().sprite = icon;
            StartCoroutine(this.ShowIcon(thoughtBubbleContent.gameObject));
        } else {
            Vector3 offset = Vector3.zero;
            Quaternion rotation = Quaternion.identity;
            if (magePos.x > 0) {
                rotation = Quaternion.Euler(0, 90, 0);
                offset = new Vector3(-0.5f, 1.25f, 0.5f);
            } else {
                rotation = Quaternion.Euler(0, -90, 0);
                offset = new Vector3(0.5f, 1.25f, -0.5f);
            }

            Instantiate(this.thoughtBubblePrefabObject, magePos + offset, rotation, mage.transform);
        }

        // zerstöre die Denbklase nach der angegebenen Zeit
        if (time > 0) StartCoroutine(this.DestroyAfter(mage, time));
    } // end Show

    /**
     * Von außerhalb aufgerufen, um die Denkblase des Magiers zu zerstören.
     */
    public void Hide(Mage mage) {
        StartCoroutine(this.DestroyBubbles(mage));
    } // end Hide

    /**
     * Lässt die Denkblase des angegebenen Magiers für time ms leben und zerstört diese dann.
     */
    private IEnumerator DestroyAfter(Mage mage, float time) {
        // warte die angegebene Zeit
        yield return new WaitForSeconds(time/1000);
        // und lass die Denkblase anschließend verschwinden
        StartCoroutine(this.DestroyBubbles(mage));
    } // end DestroyAfter

    /**
     * Zerstört alle existierenden Denkblasen eines Magiers
     */
    private IEnumerator DestroyBubbles(Mage mage) {
        for (int i = 0; i < mage.transform.childCount; i++) {
            Transform child = mage.transform.GetChild(i);
            if (child.tag == "ThoughtBubble") {
                Animator animator = child.GetComponent<Animator>();
                animator.SetBool("hideBubble", true);
                yield return new WaitForSeconds(2);
                if (child) Destroy(child.gameObject);
            }
        }
    } // end DestroyBubbles

    private IEnumerator ShowIcon(GameObject bubbleContent) {
        yield return new WaitForSeconds(0.7f);
        bubbleContent.SetActive(true);
    } // end DestroyAfter

} // end ThoughtBubbles
