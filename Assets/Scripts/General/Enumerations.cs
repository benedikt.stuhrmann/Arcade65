namespace Enums {
    public enum MagicType : int {
        None,
        Dark,
        Light
    }
    public enum BookType : int {
        Book,
        Scroll
    }
	public enum MageStatus : int {
		Bored,
		Reading,
		Casting
	}
    public enum ThoughtBubbleType : int {
        WantBook,
        Waiting
    }

    /**
     * Type Safe Enum class that sets constants for paths. Add new paths here with an incrementing int value and the path string.
     * To use these constants you have to import the Enums namespace ('using Enums;'). After that you can access a path with 'Path.PATHNAME;'.
     * This will return a string.
     * */
    public sealed class Path {
        private readonly string name;
        private readonly int value;

        public static readonly Path GRAPHICS = new Path(0, "Graphics/");

        private Path(int value, string name) {
            this.name = name;
            this.value = value;
        }

        /**
         * Use the ToString() method when using a path variable in a string context: Debug.Log("This is a path: " + Path.PATHNAME.ToString());
         * A call without this method can only be used in a single statement context: Debug.Log(Path.PATHNAME);
         * */
        public override string ToString() {
            return name;
        }
    }

    /**
     * Use this class for creating objects, that should be available with global visibilty but shouldn't get created over and over again (because of seeds, system constants or creation effort). 
     * i.e.: A random object cannot be used when creating different random values in a very short time (for example in less then a few system timer refreshes) because the same seed will be used
     * for these objects which will result in same values.
     * */
    public static class GlobalObjects {
        // this random object can be used when generating different values in a for-loop when Random.value is inpractical to use (i.e. when you want to produce numbers in a specific range)
        public static System.Random randomInstance = new System.Random();
    }
}