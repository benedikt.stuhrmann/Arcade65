﻿using UnityEngine;

public class WorldManager : MonoBehaviour {

    private GameObject[] wizardSpawns;
    // Spawnrate of the wizards in seconds
    [SerializeField]
    private float wizardSpawnRate = 5f;
    private float nextSpawnTime = 0f;
    [SerializeField]
    private GameObject wizardPrefab;
    [SerializeField]
    private MagicSchool darkMagicSchool;
    [SerializeField]
    private MagicSchool lightMagicSchool;

    // Use this for initialization
    void Start () {
        wizardSpawns = GameObject.FindGameObjectsWithTag("Wizard Spawn");
        this.darkMagicSchool = this.darkMagicSchool ?? GameObject.FindGameObjectWithTag("Magic School Dark").GetComponent<MagicSchool>();
        this.lightMagicSchool = this.lightMagicSchool ?? GameObject.FindGameObjectWithTag("Magic School Light").GetComponent<MagicSchool>();
    }
	
	// Update is called once per frame
	void Update () {
        // TODO: Im späteren Version sollte der Spawn eines neuen Zauberlehrlings nicht mehr ausschließlich von der Zeit abhängen. Es sollten eventuell Dinge wie Geschwindigkeit und Skill der Spieler mit bedacht werden.
        if (Time.time > this.nextSpawnTime) {
            this.nextSpawnTime += this.wizardSpawnRate;
            this.spawnWizard();
        }
    }

    /**
     * Spawnt einen Zauberlehrling an einem der beiden Spawnpunkte (oben oder unten) an einer zufälligen Position, die sich außerhalb des Sichtbereichs der
     * Spieler befindet.
     * @return : GameObject des neu gespawnten Wizards (wird eventuell zur Weiterverarbeitung gebraucht?)
     **/
    private GameObject spawnWizard() {
        System.Random random = new System.Random();
        int spawnArea = random.Next(0, 2);
        float newWizardPosX = this.wizardSpawns[spawnArea].transform.position.x + random.Next(-10, 11);
        float newWizardPosZ = this.wizardSpawns[spawnArea].transform.position.z + random.Next(-3, 4);
        return (GameObject) Instantiate(this.wizardPrefab, new Vector3(newWizardPosX, 1f, newWizardPosZ), Quaternion.identity);
    }

    /**
     * Entscheidet aufgrund von besetzten Sitzplätzen und Mages in der Warteschlange, welche Schule als nächstes einen Mages bekommen sollte.
     * @return : MagicSchool als Ziel für den Mage
     * */
    public MagicSchool ChooseSchool() {
        MagicSchool chosenSchool = null;
        int darkFillStatus = this.darkMagicSchool.GetInsideMagesCount();
        int lightFillStatus = this.lightMagicSchool.GetInsideMagesCount();
        int darkWaitingStatus = this.darkMagicSchool.GetWaitingMagesCount();
        int lightWaitingStatus = this.lightMagicSchool.GetWaitingMagesCount();
        int maxMages = this.lightMagicSchool.GetSeatsCount();

        if ((darkFillStatus > maxMages - 2 && lightFillStatus > maxMages - 2) || darkFillStatus == lightFillStatus) {
            if(darkWaitingStatus > lightWaitingStatus) {
                chosenSchool = this.lightMagicSchool;
            } else if(lightWaitingStatus > darkWaitingStatus) {
                chosenSchool = this.darkMagicSchool;
            } else {
                if (Random.value > 0.5) {
                    chosenSchool = this.darkMagicSchool;
                } else {
                    chosenSchool = this.lightMagicSchool;
                }
            }
        } else if(darkFillStatus > lightFillStatus) {
            chosenSchool = this.lightMagicSchool;
        } else {
            chosenSchool = this.darkMagicSchool;
        }
        return chosenSchool;
    }
}
