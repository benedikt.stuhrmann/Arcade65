﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchInteraction {

	public Vector3 startPosition;
	public Vector3 endPosition;
    public Vector3 currentPosition;
	public float speed;
    public TouchType touchType;
    public float timeTouched = 0;
    public float lastPressure = 1;
    public Vector3 direction;
    public float distance;
    public List<Vector3> movementDelta = new List<Vector3>();
    public int fingerID;
    public GameObject inUseBy = null;


    public override string ToString()
    {
        string movementDeltaText = "";
        movementDelta.ForEach(vector => { movementDeltaText += " " + vector; });
        return "startPosition: " + startPosition + "\n" +
               "endPosition: " + endPosition + "\n" +
               "speed: " + speed + "\n" +
               "touchType: " + touchType + "\n" +
               "timeTouched: " + timeTouched + "\n" +
               "lastPressure: " + lastPressure + "\n" +
               "movementDelta: " + movementDeltaText;
    }



}
