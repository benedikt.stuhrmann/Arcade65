﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchArea : MonoBehaviour {

    public List<GameObject> touchables = new List<GameObject>();
    public int touchableLayer = 1 << 30;
    public Camera cam;

    // Use this for initialization
    void Start () {
        Transform[] children = transform.GetComponentsInChildren<Transform>();
        foreach (Transform child in children) {
            if (child.CompareTag("Touchable")) {
                touchables.Add(child.gameObject);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void isTouched(TouchInteraction interaction)
    {
        List<GameObject> touchedObjects = GetTouchedGameObjects(interaction);
        if(interaction.inUseBy != null)
        {
            interaction.inUseBy.SendMessage("isTouched",interaction);
        }
        else
        {
            touchables.ForEach(touchable =>
            {
                if (touchedObjects.Contains(touchable))
                {

                    touchable.SendMessage("isTouched", interaction);
                }
            });
        }
    }

    private List<GameObject> GetTouchedGameObjects(TouchInteraction interaction)
    {
        Ray ray = cam.ScreenPointToRay(interaction.currentPosition);
        RaycastHit[] hits = Physics.RaycastAll(ray.origin, ray.direction, 100,touchableLayer);
        List<GameObject> gameObjects = new List<GameObject>();
        foreach(RaycastHit hit in hits)
        {
            gameObjects.Add(hit.collider.gameObject);
        }
        return gameObjects;
    }

    public void wasTouched(TouchInteraction interaction)
    {
        List<GameObject> touchedObjects = GetTouchedGameObjects(interaction);
        if (interaction.inUseBy != null && touchedObjects.Contains(interaction.inUseBy))
        {
            interaction.inUseBy.SendMessage("wasTouched",interaction);
        }
        else
        {
            touchables.ForEach(touchable =>
            {
                if (touchedObjects.Contains(touchable))
                {
                    touchable.SendMessage("wasTouched", interaction);
                }
            });
        }
        
    }

}
