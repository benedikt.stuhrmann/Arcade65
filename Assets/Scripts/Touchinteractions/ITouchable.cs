﻿using UnityEngine;
using UnityEditor;

public interface ITouchable 
{

    TouchActionHandler touchActionHandler { get; set; }

    /// <summary>
    /// Is called when an Ongoing Touch started on the particular object.
    /// Ongoing means the touch is not ended yet by the user. 
    /// The interaction will therefore have no endPosition (= 0,0,0).
    /// </summary>
    /// <param name="interaction">TouchInteraction </param>
    void isTouched(TouchInteraction interaction);


    /// <summary>
    /// Is called when an finished Touch started on the particular object.
    /// Finished means the user lifted his finger from the display.
    /// The interaction will therefore have an end- and startPosítion.
    /// </summary>
    /// <param name="interaction">TouchInteraction</param>
    void wasTouched(TouchInteraction interaction);
   
}