﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManager : MonoBehaviour {

    public List<GameObject> touchAreas = new List<GameObject>();
    private Dictionary<int, TouchInteraction> activeTouches = new Dictionary<int, TouchInteraction>();
    private List<TouchInteraction> finishedTouches = new List<TouchInteraction>();
    public bool pressureMeasurementIsEnabled = false;
    public bool enableMouseInteraction = false;
    [SerializeField]
    private bool debug = false;
    





    void Start()
    {
        pressureMeasurementIsEnabled = Input.touchPressureSupported;

        foreach (GameObject o in GameObject.FindGameObjectsWithTag("TouchArea"))
        {
            touchAreas.Add(o);
        }

    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch touch = Input.GetTouch(i);
            exermineTouch(touch);
        }

        if(enableMouseInteraction)
        {
            mockMouseInputToTouch();
        }

        //active Touches
        foreach (TouchInteraction touch in activeTouches.Values)
        {
            if (touch.touchType == TouchType.Ongoing)
            {
                touchAreas.ForEach(area =>
                {
                    RaycastHit hitInfo; 
                    if(area  == GetTouchedArea(out hitInfo, touch,area.GetComponent<TouchArea>()))
                    {
                        area.SendMessage("isTouched", touch);
                    }

                });
            }
        }

        //finished Touches
        finishedTouches.ForEach(touch =>
        {
            touchAreas.ForEach(area =>
            {
                RaycastHit hitInfo;
                if (area == GetTouchedArea(out hitInfo, touch,area.GetComponent<TouchArea>()))
                {
                    determineTouchType(touch,area.transform.rotation);
                    area.SendMessage("wasTouched", touch);
                }
            });
        });

        finishedTouches.Clear();

    }

    private void mockMouseInputToTouch()
    {
        if (Input.GetMouseButtonDown(0))
        {
            TouchInteraction interaction = new TouchInteraction
            {
                startPosition = Input.mousePosition,
                touchType = TouchType.Other,
                fingerID = 100
            };
            activeTouches.Add(100, interaction);
            interaction.currentPosition = interaction.startPosition;
            interaction.timeTouched += Time.deltaTime;

        }
        if (Input.GetMouseButton(0))
        {
            TouchInteraction interaction = activeTouches[100];
            interaction.timeTouched += Time.deltaTime;
            interaction.movementDelta.Add(Input.mousePosition - interaction.currentPosition);
            interaction.touchType = TouchType.Ongoing;
            interaction.currentPosition = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {

            TouchInteraction interaction = activeTouches[100];
            interaction.endPosition = Input.mousePosition;
            interaction.movementDelta.Add(Input.mousePosition - interaction.currentPosition);
            if (pressureMeasurementIsEnabled)
            {
                interaction.lastPressure = 1;
            }
            interaction.direction = interaction.endPosition - interaction.startPosition;
            interaction.distance = interaction.direction.magnitude;
            interaction.speed = Vector3.Distance(interaction.startPosition, interaction.endPosition) / interaction.timeTouched;
            activeTouches.Remove(100);
            finishedTouches.Add(interaction);
        }
    }

    private GameObject GetTouchedArea(out RaycastHit hitInfo,TouchInteraction touch,TouchArea area)
    {
        GameObject touchedGameObject = null;
        Ray ray = area.cam.ScreenPointToRay(touch.currentPosition);
        if(Physics.Raycast(ray.origin,ray.direction * 50, out hitInfo))
        {
            touchedGameObject = hitInfo.collider.gameObject;
        }

        return touchedGameObject;
    }



    private void exermineTouch(Touch touch)
    {
        if (touchHasBegun(ref touch))
        {
            TouchInteraction interaction = new TouchInteraction
            {
                startPosition = touch.position,
                touchType = TouchType.Other,
                fingerID = touch.fingerId
            };

            interaction.timeTouched += touch.deltaTime;
            activeTouches.Add(touch.fingerId, interaction);
        }
        else if (touchHasMoved(ref touch))
        {
            TouchInteraction interaction = addDeltaTimeToInteraction(ref touch);
            interaction.movementDelta.Add(touch.deltaPosition);
            interaction.touchType = TouchType.Ongoing;
            interaction.currentPosition = touch.position;
        }
        else if (touchIsHoldOnTheSamePos(ref touch))
        {
            TouchInteraction interaction = addDeltaTimeToInteraction(ref touch);
            interaction.movementDelta.Add(touch.deltaPosition);
            interaction.touchType = TouchType.Ongoing;
            interaction.currentPosition = touch.position;

        }
        else if (touchHasEnded(ref touch))
        {
            TouchInteraction interaction = addDeltaTimeToInteraction(ref touch);
            interaction.endPosition = touch.position;
            interaction.movementDelta.Add(touch.deltaPosition);
            if (pressureMeasurementIsEnabled)
            {
                interaction.lastPressure = touch.pressure;
            }
            interaction.direction = interaction.endPosition - interaction.startPosition;
            interaction.distance = interaction.direction.magnitude;
            interaction.speed = Vector3.Distance(interaction.startPosition, interaction.endPosition) / interaction.timeTouched;
            activeTouches.Remove(touch.fingerId);
            finishedTouches.Add(interaction);

        }
    }

    private TouchInteraction addDeltaTimeToInteraction(ref Touch touch)
    {
        TouchInteraction interaction = activeTouches[touch.fingerId];
        interaction.timeTouched += touch.deltaTime;
        return interaction;
    }


    private bool touchIsHoldOnTheSamePos(ref Touch touch)
    {
        return touch.phase == TouchPhase.Stationary;
    }

    private void determineTouchType(TouchInteraction interaction,Quaternion areaRotation)
    {
        if (interaction.timeTouched < 1 && Vector3.Distance(interaction.startPosition, interaction.endPosition) < 10)
        {
            interaction.touchType = TouchType.Tap;
            if (this.debug) Debug.Log("it's a t(r)ap!");
        }
        else if(Vector3.Distance(interaction.startPosition, interaction.endPosition) > 10)
        {
            if (this.debug) Debug.Log("It could be a swipe?!");
            Vector3 rotationDependentDirection = areaRotation * interaction.direction;
            determineSwipeDirection(rotationDependentDirection, interaction);
        }
        else 
        {
            interaction.touchType = TouchType.Other;
        }
    }

    private void determineSwipeDirection(Vector3 rotationDependentDirection, TouchInteraction interaction)
    {
        float movementX = 0;
        float movementY = 0;
        interaction.movementDelta.ForEach(delta =>
        {
            movementX += delta.x;
            movementY += delta.y;
        });

        bool isHorizontal = Math.Abs(movementX) > Math.Abs(movementY);
        if(isHorizontal)
        {
            if(movementX > 0)
            {
                interaction.touchType = TouchType.SwipeEast;
                if (this.debug) Debug.Log("its a swipe east");
            }
            else
            {
                interaction.touchType = TouchType.SwipeWest;
                if (this.debug) Debug.Log("its a swipe west");
            }
        }
        else
        {
            if (movementY > 0)
            {
                interaction.touchType = TouchType.SwipeNorth;
                if (this.debug) Debug.Log("its a swipe north");

            }
            else
            {
                interaction.touchType = TouchType.SwipeSouth;
                if (this.debug) Debug.Log("its a swipe south");

            }
        }
    }

    private bool touchHasEnded(ref Touch touch)
    {
        return touch.phase == TouchPhase.Ended && activeTouches.ContainsKey(touch.fingerId);
    }

    private bool touchHasMoved(ref Touch touch)
    {
        return touch.phase == TouchPhase.Moved && activeTouches.ContainsKey(touch.fingerId);
    }

    private bool touchHasBegun(ref Touch touch)
    {
        return touch.phase == TouchPhase.Began && !activeTouches.ContainsKey(touch.fingerId);
    }
}
