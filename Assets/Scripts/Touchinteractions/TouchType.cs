﻿using System;

public enum TouchType
{
	Tap,
    SwipeNorth,
    SwipeWest,
    SwipeEast,
    SwipeSouth,
    SwipeNorthEast,
    SwipeNorthWest,
    SwipeSouthEast,
    SwipeSouthWest,
    Ongoing,
    Other
}
