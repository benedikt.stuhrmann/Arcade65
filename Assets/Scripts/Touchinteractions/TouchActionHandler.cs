﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;

public class TouchActionHandler
{
    public void dragObject(GameObject gameObject, TouchInteraction interaction)
    {
        TouchArea area = gameObject.GetComponentInParent<TouchArea>();
        Camera cam = area.cam;
        Vector3 objectsScreenSpace = cam.WorldToScreenPoint(gameObject.transform.position);
        Vector3 screenPoint = cam.ScreenToWorldPoint(new Vector3(interaction.currentPosition.x, interaction.currentPosition.y, objectsScreenSpace.z));
        Vector3 goalPoint = new Vector3(screenPoint.x, gameObject.transform.position.y, screenPoint.z);
        
        // only drag book inside touch area 
        if(goalPointIsTouchArea(area,goalPoint))
        {
            gameObject.transform.position = goalPoint;
        }
            
    }

    private bool goalPointIsTouchArea(TouchArea area, Vector3 goalPoint)
    {
        return area.GetComponent<Collider>().bounds.Contains(goalPoint);
    }
}