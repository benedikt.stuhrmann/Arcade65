﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereObject : MonoBehaviour, ITouchable{

    public TouchActionHandler touchActionHandler
    {
        get;
        set;
    }

    public void isTouched(TouchInteraction interaction)
    {
        // dont needed here
    }

    public void wasTouched(TouchInteraction interaction)
    {
        if(interaction.touchType == TouchType.Tap)
        {
            Color random = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            Renderer renderer = GetComponent<Renderer>();
            renderer.material.shader = Shader.Find("_Color");
            renderer.material.SetColor("_Color", random);

            renderer.material.shader = Shader.Find("Specular");
            renderer.material.SetColor("_SpecColor", random);
        }
    }

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
