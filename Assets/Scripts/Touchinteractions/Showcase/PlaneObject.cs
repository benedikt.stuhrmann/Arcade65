﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneObject : MonoBehaviour,ITouchable {

    private TextMesh textMesh;

    public TouchActionHandler touchActionHandler
    {
        get;
        set;
    }

    public void isTouched(TouchInteraction interaction)
    {
        // nothing
    }

    public void wasTouched(TouchInteraction interaction)
    {
        switch(interaction.touchType)
        {
            case TouchType.SwipeEast:
                textMesh.text = "right";
                break;
            case TouchType.SwipeWest:
                textMesh.text = "left";
                break;
            case TouchType.SwipeNorth:
                textMesh.text = "up";
                break;
            case TouchType.SwipeSouth:
                textMesh.text = "down";
                break;
        }
    }

    // Use this for initialization
    void Start () {
        textMesh = this.GetComponentInChildren<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
