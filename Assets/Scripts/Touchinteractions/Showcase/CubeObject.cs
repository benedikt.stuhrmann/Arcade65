﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeObject : MonoBehaviour,ITouchable {
    public TouchActionHandler touchActionHandler
    {
        get;
        set;
    }

    // Use this for initialization
    void Start () {
        touchActionHandler = new TouchActionHandler();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void isTouched(TouchInteraction interaction)
    {
        if(interaction.touchType == TouchType.Ongoing)
        {
            touchActionHandler.dragObject(this.gameObject, interaction);
        }
    }

    public void wasTouched(TouchInteraction interaction)
    {

    }

    
}
