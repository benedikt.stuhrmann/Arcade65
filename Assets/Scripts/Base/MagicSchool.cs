﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using Enums;

public class MagicSchool : MonoBehaviour {

    // Legt den Magietyp fest, der in dieser Zauberschule gelehrt wird.
    [SerializeField]
    private MagicType magicType = Enums.MagicType.Light;

    [SerializeField]
    private int maxHealth = 1000;
    private float health;

    // Warteschlange mit Zauberern, die vor der Zauberschule warten.
    private Queue<Mage> waiting;

    // Liste mit Zauberern, die gerade in der Zauberschule sitzen.
    private Mage[] inside;
    // Die (Positionen/Transforms der) Sitzplätze in der Zauberschule
    private List<Transform> seats = new List<Transform>();

    [SerializeField]
    private GameObject bookPrefab;
    // Alle Bücher, welche dem Spieler derzeit zum Austeilen zur Verfügung stehen.
    public Book[] books;
    // Die (Positionen/Transforms der) Spawnpunkte für die Bücher
    private List<Transform> bookSpawns = new List<Transform>();

    [SerializeField]
    private Bookshelf[] bookshelves;

    [SerializeField]
    private Camera cam;
    [SerializeField]
    private Canvas canvas;
    private Slider healthbar;

	void Start () {
        this.health = this.maxHealth;

        this.waiting = new Queue<Mage>();

        Transform[] children = transform.GetComponentsInChildren<Transform>();
        foreach (Transform child in children) {
            // Überprüfe, wie viele Sitzplätze es gibt (= Anzahl der Seats)
            if (child.CompareTag("Seat")) {
                this.seats.Add(child);
            }

            // Speicher die Bücherspawns für zukünftige Verwendung ab
            if (child.CompareTag("Book Spawn")) {
                this.bookSpawns.Add(child);
            }
        }

        // Bücherregale müssen von MagicSchool erstellt werden, um sicher zu gehen, dass alle nötigen variablen von MagicSchool bereits gesetzt sind
        foreach(Bookshelf shelf in this.bookshelves) {
            shelf.Initiate();
        }

        // erstelle Array für Magier in der Zauberschule
        this.inside = new Mage[this.seats.Count];

        // erstelle und befülle Bücher Spawns (= Anzahl der Bookspawns)
        this.books = new Book[this.bookSpawns.Count];
        this.RestockBooks();

        if (this.canvas != null) {
            this.healthbar = this.canvas.GetComponentInChildren<Slider>();
            this.healthbar.minValue = 0;
            this.healthbar.maxValue = this.maxHealth;
            this.healthbar.value = this.maxHealth;
        }
    } // end Start
	

	void Update () {
        this.RestockBooks();
        
        // TODO: vielleicht nur alle 2 Sekunden öä. überprüfen? => Performance
        if(this.waiting.Count > 0) {
            // wenn es wartende Zauberer gibt, überprüfe, ob der Erste eintreten kann
            CanIEnter(this.waiting.Peek(), false);
        }
    } // end Update

    public void SetMagicType(MagicType _magicType) {
        this.magicType = _magicType;
    } // end setMagicType

    public MagicType GetMagicType() {
        return this.magicType;
    } // end getMagicType

    public void TakeDamage(float damage) {
        this.health -= damage;
        this.healthbar.value = this.health;
        if (this.health <= 0) {
            // Welt benachrichtigen, dass zerstört
            Debug.Log(this.name + " wurde zerstört");
        }
    } // end takeDamage

    /**
     * Fragt an, ob der übergebene Zauberer die Zauberschule betreten kann.
     * 
     * Wenn es einen freien Platz gibt, wird beim Zauberer die Funktion (TODO: welche Methode?) aufgerufen, 
     * welche ihm die Koordinaten mitteilt, an die er gehen soll.
     * 
     * Gibt es derzeit keinen freien Platz, wird der Zauberer in eine Warteschlange aufgenommen und 
     * benachrichtig (Aufruf der Methode (TODO: welche Methode?)) sobald ein Platz frei ist und er der am längsten wartende Zauberer ist.
     */
    public void CanIEnter(Mage mage, bool enqueue = true) {
        int freeIndex = FreeIndex(this.inside);

        if (freeIndex != -1) {
            // wenn Platz ist, reinbitten
            this.inside[freeIndex] = mage;
            mage.SetSeatIndex(freeIndex);

            // Wenn Anfrage aus der Warteschlange => daraus entfernen
            if (!enqueue) this.waiting.Dequeue();

            // Sitzplatz Position ermitteln
            Vector3 seatPos = this.seats.ElementAt(freeIndex).position;
            seatPos.y += 1f;

            // Magier auffordern zu Sitzplatz zu gehen
            // TODO: (nicht die finale Funktion) => mage.goTo(x,y); / mage.setDestination(x,y);
            mage.GoTo(seatPos);
        }
        else if (enqueue) {
            // sonst in Warteschlange einfügen und benachrichtigen, sobald Platz ist
            this.waiting.Enqueue(mage);
        }
    } // end CanIEnter

    /**
     * Wird von einem Zauberer aufgerufen, um sich aus der Zauberschule abzumelden.
     * Sein Sitzplatz wird dann für den nächsten freigegeben.
     */
    public void FreeUpSeat(Mage mage) {
        for (int i = 0; i < this.inside.Length; i++) {
            if (this.inside[i] == mage) this.inside[i] = null;
        }
        Destroy(mage.gameObject);
    } // end FreeUpSeat

    public void KickOut(int index) {
        if (index > -1 && index < this.inside.Length && this.inside[index] != null) {
            GameObject mageGo = this.inside[index].gameObject;
            mageGo.GetComponent<Mage>().Leave();
        }
    } // end KickOut

    /**
     * Gibt den Index des ersten freien (= null) Felds im Array zurück.
     * Gibt -1 zurück, wenn alle Arrayfelder belegt sind.
     */
    private int FreeIndex(object[] array) {
        for (int i = 0; i < array.Length; i++){
            if (array[i] == null) return i;
        }
        return -1;
    } // end FreeIndex


    /****************
     * Zauberbuch-relatierte Methoden und Funktionen
     ****************/
    /**
    *  Füllt den Büchervorrat auf, welcher dem Spieler zum Austeilen zur Verfügung steht.
    *  (Maximal alle this.bookSpawnRate ms ein neues Buch)
    */
    private void RestockBooks() {
        int i = this.FreeIndex(this.books);
        if (i != -1 && i < this.books.Length) {
            // An der Position des i-ten Spawns ein Buch spawnen
            this.SpawnBook(i);
        }
    } // end restockBooks

    /**
     * Spawnt ein Buch an dem angegebenen Index (vom Bookspawn).
     */
    private void SpawnBook(int spawnIndex) {
        // Position/Rotation des Bücherspawns ermitteln
        object[] posRot = this.GetBookSpawnPosRot(spawnIndex);
        Vector3 bookSpawnPos = (Vector3) posRot[0];
        Quaternion bookSpawnRot = (Quaternion) posRot[1];

        // ein Buch aus dem Regal nehmen. Momentan noch experimentel einfach aus dem ersten Regal.
        // TODO: Später könnte es mehrere Regale geben, dann könnte entweder ein Regal zufällig ausgewählt werden, oder es wird zuerst das erste Regal leer gemacht?
        GameObject bookToSpawn = this.bookshelves[0].TakeBook();
        // als Kind des entsprechendes Bücherspawns erstellen
        this.books[spawnIndex] = Instantiate(this.bookPrefab, bookToSpawn.transform.position, bookToSpawn.transform.rotation, this.bookSpawns.ElementAt(spawnIndex)).GetComponent<Book>();
        // Überschreibe die Werte des neugespawnten Buches mit den Werten aus dem Bücherregal
        this.books[spawnIndex].SetMagicType(bookToSpawn.GetComponent<Book>().GetMagicType(), true);
        this.books[spawnIndex].SetBookType(bookToSpawn.GetComponent<Book>().GetBookType(), true);
        this.books[spawnIndex].SetWisdom(bookToSpawn.GetComponent<Book>().GetWisdom());
        this.books[spawnIndex].SetSpawnIndex(spawnIndex);
        this.books[spawnIndex].StartMoving(bookSpawnPos, this.magicType.ToString());
    } // end SpawnBook

    /// <summary>
    /// Gibt Position und Rotation des gewünschten Bücherspawns zurück
    /// </summary>
    /// <param name="spawnIndex">Der Index des Bücherspawns</param>
    /// <returns>object[0] = Vector3 pos und object[1] = Quaternion rot</returns>
    public object[] GetBookSpawnPosRot(int spawnIndex) {
        object[] posRot = new object[2];

        Vector3 pos = this.bookSpawns.ElementAt(spawnIndex).position;
        pos.y += 0.2f;
        posRot[0] = pos;

        Quaternion rot = this.bookSpawns.ElementAt(spawnIndex).rotation;
        posRot[1] = rot;

        return posRot;
    } // end GetBookSpawnPosRot

    /**
     * Zerstört ein Buch.
     * */
    public void DestroyBook(GameObject book) {
        int index = Array.IndexOf<Book>(this.books, book.GetComponent<Book>());
        Destroy(book);
        if (index > -1) StartCoroutine(FreeBookIndex(index));
    }

    public void MoveBookToReadPoint(GameObject book) {
        Book toRead = book.GetComponent<Book>();
        int spawnIndex = toRead.GetReader().GetSeatIndex();
        Vector3 readPointPos = new Vector3(100, 100, 100);
        foreach (Transform child in this.seats[spawnIndex]) {
            if(child.tag.Equals("BookLearnPoint")) {
                readPointPos = child.position;
            }
        }
        book.transform.position = readPointPos;
        int index = Array.IndexOf<Book>(this.books, book.GetComponent<Book>());
        if (index > -1) StartCoroutine(FreeBookIndex(index));
        book.transform.parent = this.seats[spawnIndex];
    }

    public void FinishAndDestroyBook(int bookIndex) {
        foreach(Transform child in this.seats[bookIndex]) {
            if(child.tag.Equals("Book")) {
                Destroy(child.gameObject);
            }
        }
    }

    private IEnumerator FreeBookIndex(int index) {
        yield return new WaitForSeconds(0.5f);
        this.books[index] = null;
    } // end FreeBookIndex

    private IEnumerator FreeSeatIndex(int index) {
        yield return new WaitForSeconds(1.5f);
        this.inside[index] = null;
    } // end FreeSeatIndex

    public List<Transform> GetBookSpawns() {
        return this.bookSpawns;
    }

    public Mage[] getInsideMages() {
        return this.inside;
    }

    public int GetInsideMagesCount() {
        int counter = 0;
        foreach(Mage m in this.inside) {
            if(m != null && m.GetType() == new Mage().GetType()) {
                counter++;
            }
        }
        return counter;
    }

    public int GetWaitingMagesCount() {
        return this.waiting.Count;
    }

    public int GetSeatsCount() {
        return this.seats.Count;
    }

} // end MagicSchool
